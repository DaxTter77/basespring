
package co.bmsoftware.components.dao.persistence;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import co.bmsoftware.components.dao.entities.ForaneaDTO;
import co.bmsoftware.components.dao.persistence.oracle.GenericOracleDAO;
import co.bmsoftware.utils.Utilidades;

@Repository
public class GenericDAO extends GenericOracleDAO {

	protected static final Logger bitacora = LogManager.getLogger();
	// NOMBRE DE LA APLICAICON CON LA SE REGISTRA EN EL LOG DE USO
	private static final String NOMBRE_APLICACION = "EJEMPLO";

	@Override
	public void create(Object entity, String sql) throws Exception {
		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
		this.getNamedParameterJdbcTemplate().update(sql, namedParameters);

	}

	@Override
	public void update(Object entity, String sql) throws Exception {
		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
		this.getNamedParameterJdbcTemplate().update(sql, namedParameters);
	}

	@Override
	public void delete(Object entity, String sql) throws Exception {
		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
		this.getNamedParameterJdbcTemplate().update(sql, namedParameters);
	}

	@Override
	public List findByCriteria(Object entity, String sql, String order, boolean isDesc) throws Exception {
		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
		if (!Utilidades.StringUtil.isEmptyOrNull(order)) {
			sql += " order by " + order + (isDesc ? " desc" : " asc");
		}
		return this.getNamedParameterJdbcTemplate().query(sql, namedParameters,
				new BeanPropertyRowMapper(entity.getClass()));
	}

	@Override
	public Object findByPK(Object entity, String sql) throws Exception {
		List results = null;
		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
		results = this.getNamedParameterJdbcTemplate().query(sql, namedParameters,
				new BeanPropertyRowMapper(entity.getClass()));
		if (results != null && results.size() > 0) {
			return results.get(0);
		}
		return null;
	}

	public void registerLog(String usuario, String emplid, String accion, String texto) throws Exception {
		Map<String, Object> parameters = null;
		String sql = null;
		sql = "INSERT INTO PUJ_SYSADM.LOG_USUAPP (FECHA,USUARIO,EMPLID,APLICACION,ACCION,TEXTO) "
				+ "VALUES (:fecha, :usuario, :emplid, :aplicacion, :accion, :texto)";
		parameters = new HashMap<String, Object>();
		parameters.put("fecha", new Timestamp(System.currentTimeMillis()));
		parameters.put("usuario", usuario);
		parameters.put("emplid", emplid);
		parameters.put("aplicacion", NOMBRE_APLICACION);
		parameters.put("accion", accion);
		if (texto == null) {
			parameters.put("texto", "");
		} else {
			parameters.put("texto", texto);
		}

		this.getNamedParameterJdbcTemplate().update(sql, parameters);

	}

	@Override
	public Connection getDataConnection() throws Exception {
		return getConnection();
	}

	@Override
	public List<Map<String, Object>> findByCriteriaGeneral(String sql, Map<String, ?> parametros) throws Exception {
		return this.getNamedParameterJdbcTemplate().queryForList(sql, parametros);
	}

	@Override
	public Map<String, Object> findByPKGeneral(String sql, Map<String, ?> parametros) throws Exception {
		List<Map<String, Object>> results = null;
		results = this.getNamedParameterJdbcTemplate().queryForList(sql, parametros);
		if (results != null && results.size() > 0) {
			return results.get(0);
		}
		return null;
	}

	@Override
	public List findByCriteriaForeign(Object entity, String tabla, String select, String where,
			List<ForaneaDTO> foraneasList, HashMap<String, String> aliasForaneas) throws Exception {
		List results = null;
		String sql = null;
		StringBuilder selectStatement = new StringBuilder("select ");
		StringBuilder fromStatement = new StringBuilder("from " + tabla);
		String[] arrayCampos;
		String aliasCampoForeing = null;

		// se agregua el nombre de la tabla padre a sus respectivos campos
		arrayCampos = select.split(",");
		for (int i = 0; i < arrayCampos.length; i++) {
			selectStatement.append(tabla + "." + arrayCampos[i]);
			selectStatement.append(i != (arrayCampos.length - 1) ? "," : "");// se
																				// determina
																				// si
																				// es
																				// el
																				// ultimo
																				// campo
																				// para
																				// ponerle
																				// una
																				// coma
		}
		// Ajustamos el select con los campos que se requieren de las
		// foraneas
		for (int j = 0; j < foraneasList.size(); j++) {
			arrayCampos = foraneasList.get(j).getCampos().split(",");
			selectStatement.append(",");
			for (int i = 0; i < arrayCampos.length; i++) {
				aliasCampoForeing = aliasForaneas.get(foraneasList.get(j).getTabla()) + "_" + arrayCampos[i];
				// if(aliasCampoForeing.length()>30);
				// aliasCampoForeing=aliasCampoForeing.substring(0,30);

				selectStatement.append(foraneasList.get(j).getTabla() + "." + arrayCampos[i] + " " + aliasCampoForeing);// se
																														// pone
																														// en
																														// el
																														// select
																														// los
																														// campos
																														// de
																														// la
																														// foranea
																														// y
																														// su
																														// respectivo
																														// aleas
				selectStatement.append(i != (arrayCampos.length - 1) ? "," : "");// se determina si es el ultimo campo
																					// para
																					// ponerle una coma
			}
			// creamos el join
			fromStatement.append(" JOIN " + foraneasList.get(j).getTabla() + " ON " + tabla + "."
					+ foraneasList.get(j).getCampoForanea() + "=" + foraneasList.get(j).getTabla() + "."
					+ foraneasList.get(j).getIdForanea());
		}

		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
		sql = selectStatement.toString() + " " + fromStatement.toString() + " " + (where == null ? "1=1" : where);
		results = this.getNamedParameterJdbcTemplate().query(sql, namedParameters,
				new BeanPropertyRowMapper(entity.getClass()));

		return results;
	}

	@Override
	public List executeParametizedQuery(Object entity, Long idParametro) throws Exception {
		List results = null;
		String consultaParametro = null;
		String consultaParametrizada = null;
		Map<String, Object> parametros = null;

		consultaParametro = "select valor from parametro where id_parametro= :idParametro";
		parametros = new HashMap<String, Object>();
		parametros.put("idParametro", idParametro);
		consultaParametrizada = (String) (findByPKGeneral(consultaParametro, parametros)).get("valor");

		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
		results = this.getNamedParameterJdbcTemplate().query(consultaParametrizada, namedParameters,
				new BeanPropertyRowMapper(entity.getClass()));

		return results;
	}

}
