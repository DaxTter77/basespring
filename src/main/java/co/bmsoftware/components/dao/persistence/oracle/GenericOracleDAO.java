package co.bmsoftware.components.dao.persistence.oracle;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import co.bmsoftware.interfaces.services.base.IGenericDAO;

public abstract class GenericOracleDAO extends NamedParameterJdbcDaoSupport implements IGenericDAO {
	private OracleJdbcTemplate oracleTemplate;

	@Autowired
	@Qualifier("principalDataSource")
	public void dataSource(BasicDataSource dataSource) {
		this.setDataSource(dataSource);
		oracleTemplate = new OracleJdbcTemplate(dataSource);
	}

	@Override
	public Long create(Object entity, String sql, String[] columnNames) throws Exception {
		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		this.oracleTemplate.update(sql, namedParameters, keyHolder, columnNames);
		return keyHolder.getKey() != null ? keyHolder.getKey().longValue() : 0L;
	}

}
