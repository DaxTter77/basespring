package co.bmsoftware.components.exception.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import co.bmsoftware.components.exception.dao.IAppExcepcionDAO;
import co.bmsoftware.exception.AppException;
import co.bmsoftware.interfaces.services.base.IAppExceptionService;

public class ExceptionFromTables implements IAppExceptionService {
	protected static final Logger bitacora = LogManager.getLogger();

	@Autowired
	private IAppExcepcionDAO accessData;

	@Override
	public AppException throwException(String codigo, String[] dato) {
		AppException exc = new AppException(codigo, dato);
		try {
			exc = accessData.findByPK(exc);
			// Si no se encuentra la excepcion el codigo no existe
			if (exc == null) {
				exc = new AppException();
				exc.setDescripcion(AppException.DESCRIPCION_EXCEPCION_CODIGO);
				exc.setSolucion(AppException.SOLUCION_EXCEPCION_CODIGO);
				String[] datos = new String[1];
				datos[0] = codigo;
				exc.setDato(datos);
				exc.loadData();
				return exc;
			}
			exc.setDato(dato);
			exc.loadData();
		} catch (Exception e) {
			bitacora.error("JaverianaException.throwException. Causa: " + e.getMessage() + ".");
			exc.setDescripcion(AppException.DESCRIPCION_EXCEPCION);
			exc.setSolucion(AppException.SOLUCION_EXCEPCION);
		}
		return exc;
	}

}
