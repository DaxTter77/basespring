
package co.bmsoftware.components.exception.dao;

import org.springframework.stereotype.Repository;

import co.bmsoftware.components.dao.persistence.GenericDAO;
import co.bmsoftware.exception.AppException;

@Repository
public class AppExceptionDAO extends GenericDAO implements IAppExcepcionDAO {
	private static final String ESQUEMA_TABLA_EXCEPCIONES = "";

	@Override
	public AppException findByPK(AppException entity) {
		String query = null;
		try {
			query = "select codigo, descripcion, solucion, soporte from " + ESQUEMA_TABLA_EXCEPCIONES
					+ "excepciones_app where codigo = :codigo";
			return (AppException) super.findByPK(entity, query);
		} catch (Exception e) {
			bitacora.error("AppExceptionDAO.create. Causa: " + e.getMessage() + ". Query: " + query.toString());
		}

		return null;
	}

}
