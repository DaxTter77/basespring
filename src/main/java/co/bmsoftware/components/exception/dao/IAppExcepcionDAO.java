package co.bmsoftware.components.exception.dao;

import co.bmsoftware.exception.AppException;

public interface IAppExcepcionDAO {

	/**
	 * Obtener una tupla usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws Exception
	 */
	public AppException findByPK(AppException entity);

}
