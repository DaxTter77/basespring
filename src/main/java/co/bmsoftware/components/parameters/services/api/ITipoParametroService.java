package co.bmsoftware.components.parameters.services.api;

import java.util.List;

import co.bmsoftware.components.parameters.entites.TipoParametroDTO;
import co.bmsoftware.exception.AppException;

/**
 * Clase service ITipoParametroService
 * 
 * @author bmsoftGenerator
 * @since V1.0
 */
public interface ITipoParametroService {
	/**
	 * Persistir un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a persistir
	 * @throws Exception
	 */
	public void create(TipoParametroDTO entity) throws AppException;

	/**
	 * Actiualizar un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a actualizar
	 * @throws Exception
	 */
	public void update(TipoParametroDTO entity) throws AppException;

	/**
	 * Borrar un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a borrar
	 * @throws Exception
	 */
	public void delete(TipoParametroDTO entity) throws AppException;

	/**
	 * Obtener un listado de objetos usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws Exception
	 */
	public List<TipoParametroDTO> findByCriteria(TipoParametroDTO entity) throws AppException;

	/**
	 * Obtener una tupla usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws Exception
	 */
	public TipoParametroDTO findByPK(TipoParametroDTO entity) throws AppException;

	/**
	 * Obtener una tupla usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws Exception
	 */
	public List<TipoParametroDTO> findByCriteriaForeign(TipoParametroDTO entity) throws AppException;

}
