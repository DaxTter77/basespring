package co.bmsoftware.components.parameters.services.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.bmsoftware.components.parameters.config.ParametroConfig;
import co.bmsoftware.components.parameters.entites.ParametroDTO;
import co.bmsoftware.components.parameters.persistence.api.IParametroDAO;
import co.bmsoftware.components.parameters.services.api.IParametroService;
import co.bmsoftware.exception.AppException;
import co.bmsoftware.interfaces.services.base.IAppExceptionService;

@Service("ParametroService")
public class ParametroService implements Serializable, IParametroService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static final Logger bitacora = LogManager.getLogger();

	@Autowired
	private IParametroDAO accessData;

	@Autowired
	@Qualifier("ParametroExceptionService")
	private IAppExceptionService exceptionService;

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void create(ParametroDTO entity) throws AppException {
		try {
			accessData.create(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("ParametroDTO.create. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}

	}

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void update(ParametroDTO entity) throws AppException {
		try {
			accessData.update(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("ParametroDTO.update. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}
	}

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void delete(ParametroDTO entity) throws AppException {
		try {
			accessData.delete(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("ParametroDTO.update. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}
	}

	@Override
	public List<ParametroDTO> findByCriteria(ParametroDTO entity) throws AppException {
		try {
			return accessData.findByCriteria(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("ParametroDTO.findByCriteria. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}
	}

	@Override
	public ParametroDTO findByPK(ParametroDTO entity) throws AppException {
		try {
			return accessData.findByPK(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("ParametroDTO.findByPK. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}
	}

}