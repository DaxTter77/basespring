package co.bmsoftware.components.parameters.services.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.bmsoftware.components.parameters.config.ParametroConfig;
import co.bmsoftware.components.parameters.entites.TipoParametroDTO;
import co.bmsoftware.components.parameters.persistence.api.ITipoParametroDAO;
import co.bmsoftware.components.parameters.services.api.ITipoParametroService;
import co.bmsoftware.exception.AppException;
import co.bmsoftware.interfaces.services.base.IAppExceptionService;

@Service("TipoParametroService")
public class TipoParametroService implements Serializable, ITipoParametroService {

	private static final long serialVersionUID = 1L;
	protected static final Logger bitacora = LogManager.getLogger();

	@Autowired
	@Qualifier("ParametroExceptionService")
	private IAppExceptionService exceptionService;

	@Autowired
	private ITipoParametroDAO accessData;

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void create(TipoParametroDTO entity) throws AppException {
		try {
			accessData.create(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroService.create. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}
	}

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void update(TipoParametroDTO entity) throws AppException {
		try {
			accessData.update(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroService.update. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}
	}

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void delete(TipoParametroDTO entity) throws AppException {
		try {
			accessData.delete(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroService.delete. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}
	}

	@Override
	public List<TipoParametroDTO> findByCriteria(TipoParametroDTO entity) throws AppException {
		try {
			return accessData.findByCriteria(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroService.findByCriteria. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}
	}

	@Override
	public TipoParametroDTO findByPK(TipoParametroDTO entity) throws AppException {
		try {
			return accessData.findByPK(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroService.findByPK. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}
	}

	@Override
	public List<TipoParametroDTO> findByCriteriaForeign(TipoParametroDTO entity) throws AppException {
		try {
			return accessData.findByCriteriaForeign(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroService.findByCriteriaForeign. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.SERVICE.getValue(), null);

		}
	}

}
