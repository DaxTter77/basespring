
package co.bmsoftware.components.parameters.services.api;

import java.util.List;

import co.bmsoftware.components.parameters.entites.ParametroDTO;
import co.bmsoftware.exception.AppException;

/**
 * Clase service IParametroService
 * 
 * @author bmsoftGenerator
 * @since V1.0
 */
public interface IParametroService {
	/**
	 * Persistir un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a persistir
	 * @throws Exception
	 */
	public void create(ParametroDTO entity) throws AppException;

	/**
	 * Actiualizar un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a actualizar
	 * @throws Exception
	 */
	public void update(ParametroDTO entity) throws AppException;

	/**
	 * Borrar un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a borrar
	 * @throws Exception
	 */
	public void delete(ParametroDTO entity) throws AppException;

	/**
	 * Obtener un listado de objetos usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws Exception
	 */
	public List<ParametroDTO> findByCriteria(ParametroDTO entity) throws AppException;

	/**
	 * Obtener una tupla usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws Exception
	 */
	public ParametroDTO findByPK(ParametroDTO entity) throws AppException;

}
