
package co.bmsoftware.components.parameters.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import co.bmsoftware.components.parameters.config.ParametroConfig;
import co.bmsoftware.exception.AppException;
import co.bmsoftware.interfaces.services.base.IAppExceptionService;

@Service("ParametroExceptionService")
public class ParametroExceptionService implements IAppExceptionService {

	protected static final Logger bitacora = LogManager.getLogger();

	@Override
	public AppException throwException(String codigo, String[] dato) {
		AppException exc = new AppException(codigo, dato);
		try {
//			exc =  accessData.findByPK(exc);
			// Si no se encuentra la excepcion el codigo no existe
			exc = new AppException();
			exc.setDescripcion(ParametroConfig.ComponentExceptions.valueOf(codigo.toUpperCase()).getDescripcion());
			exc.setSolucion(ParametroConfig.ComponentExceptions.valueOf(codigo.toUpperCase()).getSolucion());

			if (dato != null && dato.length > 0) {
				exc.setDato(dato);
				exc.loadData();
			}
			return exc;
		} catch (Exception e) {
			bitacora.error("ParametroException.throwException. Causa: " + e.getMessage() + ".");
			return exc;
		}
	}

}
