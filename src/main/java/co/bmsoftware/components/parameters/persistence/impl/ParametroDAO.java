package co.bmsoftware.components.parameters.persistence.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.bmsoftware.components.dao.persistence.GenericDAO;
import co.bmsoftware.components.parameters.config.ParametroConfig;
import co.bmsoftware.components.parameters.entites.ParametroDTO;
import co.bmsoftware.components.parameters.persistence.api.IParametroDAO;
import co.bmsoftware.exception.AppException;
import co.bmsoftware.interfaces.services.base.IAppExceptionService;

@Repository
public class ParametroDAO extends GenericDAO implements Serializable, IParametroDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected static final Logger bitacora = LogManager.getLogger();

	@Autowired
	@Qualifier("ParametroExceptionService")
	private IAppExceptionService exceptionService;

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void create(ParametroDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");

		ParametroDTO objDTO = new ParametroDTO();
		objDTO.setNombre(entity.getNombre());
		List<ParametroDTO> listDTO = findByCriteria(objDTO);

		try {
			sql.append("INSERT INTO " + ParametroConfig.NOMBRE_TABLA_PARAM + " (" + " ID_PARAMETRO,"
					+ " ID_TIPO_PARAMETRO," + " NOMBRE," + " DESCRIPCION," + " VALOR," + " TIPO_VALOR,"
					+ " USUARIO_CREA," + " FECHA_CREACION" + " )" + " VALUES (" + " S_PARAMETRO.NEXTVAL ,"
					+ " :idTipoParametro," + " :nombre," + " :descripcion," + " :valor," + " :tipoValor,"
					+ " :usuarioCrea," + " :fechaCreacion" + " )");

			if (listDTO == null || listDTO.size() == 0) {
				super.create(entity, sql.toString());
			} else {
				throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.NOMBRE_REPETIDO.getValue(), null);
			}

		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("ParametroDAO.create. Causa: " + e.getMessage() + ". Query: " + sql.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void update(ParametroDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");

		ParametroDTO objDTO = new ParametroDTO();
		objDTO.setNombre(entity.getNombre());
		objDTO.setIdParametroValid(entity.getIdParametro());

		try {
			sql.append("UPDATE " + ParametroConfig.NOMBRE_TABLA_PARAM + " SET " + " ID_TIPO_PARAMETRO=:idTipoParametro,"
					+ " NOMBRE=:nombre," + " DESCRIPCION=:descripcion," + " VALOR=:valor," + " TIPO_VALOR=:tipoValor,"
					+ " USUARIO_CREA=:usuarioCrea," + " FECHA_CREACION=:fechaCreacion" + " WHERE "
					+ "ID_PARAMETRO=:idParametro");

			List<ParametroDTO> listDTO = findByCriteria(objDTO);
			if (listDTO == null || listDTO.size() == 0) {
				super.update(entity, sql.toString());
			} else {
				throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.NOMBRE_REPETIDO.getValue(), null);
			}
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("ParametroDAO.update. Causa: " + e.getMessage() + ". Query: " + sql.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void delete(ParametroDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("DELETE FROM " + ParametroConfig.NOMBRE_TABLA_PARAM + " WHERE " + "ID_PARAMETRO=:idParametro");

			super.delete(entity, sql.toString());
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("ParametroDAO.delete. Causa: " + e.getMessage() + ". Query: " + sql.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}

	@Override
	public List<ParametroDTO> findByCriteria(ParametroDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("select" + " PARAMETRO.ID_PARAMETRO," + " PARAMETRO.ID_TIPO_PARAMETRO,"
					+ " TIPO.NOMBRE NOMBRE_TIPO," + " PARAMETRO.NOMBRE," + " PARAMETRO.DESCRIPCION,"
					+ " PARAMETRO.VALOR," + " PARAMETRO.TIPO_VALOR," + " PARAMETRO.USUARIO_CREA,"
					+ " PARAMETRO.FECHA_CREACION" + " FROM " + ParametroConfig.NOMBRE_TABLA_PARAM + " INNER JOIN "
					+ ParametroConfig.NOMBRE_TABLA_TIPO_PARAM
					+ " TIPO ON TIPO.ID_TIPO_PARAMETRO = PARAMETRO.ID_TIPO_PARAMETRO" + " WHERE 1=1 ");
			if (entity.getIdParametro() != null)
				sql.append(" AND PARAMETRO.ID_PARAMETRO=:idParametro");
			if (entity.getIdTipoParametro() != null)
				sql.append(" AND PARAMETRO.ID_TIPO_PARAMETRO=:idTipoParametro");
			if (entity.getNombre() != null && !entity.getNombre().equals(""))
				sql.append(" AND PARAMETRO.NOMBRE=:nombre");
			if (entity.getDescripcion() != null && !entity.getDescripcion().equals(""))
				sql.append(" AND PARAMETRO.DESCRIPCION=:descripcion");
			if (entity.getValor() != null && !entity.getValor().equals(""))
				sql.append(" AND PARAMETRO.VALOR=:valor");
			if (entity.getUsuarioCrea() != null && !entity.getUsuarioCrea().equals(""))
				sql.append(" AND PARAMETRO.USUARIO_CREA=:usuarioCrea");
			if (entity.getFechaCreacion() != null)
				sql.append(" AND PARAMETRO.FECHA_CREACION=:fechaCreacion");

			if (entity.getIdParametroValid() != null)
				sql.append(" AND PARAMETRO.ID_PARAMETRO != :idParametroValid");

			return super.findByCriteria(entity, sql.toString(), null, false);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("ParametroDAO.findByCriteria. Causa: " + e.getMessage() + ". Query: " + sql.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}

	@Override
	public ParametroDTO findByPK(ParametroDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("select" + " PARAMETRO.ID_PARAMETRO," + " PARAMETRO.ID_TIPO_PARAMETRO,"
					+ " TIPO.NOMBRE NOMBRE_TIPO," + " PARAMETRO.NOMBRE," + " PARAMETRO.DESCRIPCION,"
					+ " PARAMETRO.VALOR," + " PARAMETRO.TIPO_VALOR," + " PARAMETRO.USUARIO_CREA,"
					+ " PARAMETRO.FECHA_CREACION" + " FROM " + ParametroConfig.NOMBRE_TABLA_PARAM + " INNER JOIN "
					+ ParametroConfig.NOMBRE_TABLA_TIPO_PARAM + " TIPO ON TIPO.ID_TIPO_PARAMETRO = PARAMETRO.ID_TIPO_PARAMETRO" + " WHERE "
					+ "PARAMETRO.ID_PARAMETRO=:idParametro");

			return (ParametroDTO) super.findByPK(entity, sql.toString());
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("ParametroDAO.findByPK. Causa: " + e.getMessage() + ". Query: " + sql.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}

}
