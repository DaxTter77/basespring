package co.bmsoftware.components.parameters.persistence.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.bmsoftware.components.dao.entities.ForaneaDTO;
import co.bmsoftware.components.dao.persistence.GenericDAO;
import co.bmsoftware.components.parameters.config.ParametroConfig;
import co.bmsoftware.components.parameters.entites.TipoParametroDTO;
import co.bmsoftware.components.parameters.persistence.api.ITipoParametroDAO;
import co.bmsoftware.exception.AppException;
import co.bmsoftware.interfaces.services.base.IAppExceptionService;

@Repository
public class TipoParametroDAO extends GenericDAO implements Serializable, ITipoParametroDAO {

	private static final long serialVersionUID = 1L;
	protected static final Logger bitacora = LogManager.getLogger();
	private final String TABLA = ParametroConfig.NOMBRE_TABLA_TIPO_PARAM;

	@Autowired
	@Qualifier("ParametroExceptionService")
	private IAppExceptionService exceptionService;

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void create(TipoParametroDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("INSERT INTO " + TABLA + "(" + " ID_TIPO_PARAMETRO," + " NOMBRE," + " USUARIO_CREA," + " ACTIVO,"
					+ " FECHA_CREACION" + " )" + " VALUES (" + " :idTipoParametro,"

					+ " :nombre,"

					+ " :usuarioCrea,"

					+ " :activo,"

					+ " :fechaCreacion"

					+ " )");

			super.create(entity, sql.toString());
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroDAO.create. Causa: " + e.getMessage() + ". Query: " + sql.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void update(TipoParametroDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("UPDATE " + TABLA + " SET " + " NOMBRE=:nombre," + " USUARIO_CREA=:usuarioCrea,"
					+ " ACTIVO=:activo," + " FECHA_CREACION=:fechaCreacion" + " WHERE "
					+ "ID_TIPO_PARAMETRO=:idTipoParametro");

			super.update(entity, sql.toString());
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroDAO.update. Causa: " + e.getMessage() + ". Query: " + sql.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void delete(TipoParametroDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("DELETE FROM " + TABLA + " " + " WHERE " + "ID_TIPO_PARAMETRO=:idTipoParametro");

			super.delete(entity, sql.toString());
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroDAO.delete. Causa: " + e.getMessage() + ". Query: " + sql.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}

	@Override
	public List<TipoParametroDTO> findByCriteria(TipoParametroDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("select" + " ID_TIPO_PARAMETRO," + " NOMBRE," + " USUARIO_CREA," + " ACTIVO," + " FECHA_CREACION"
					+ " FROM " + TABLA + " " + " WHERE 1=1 ");
			if (entity.getIdTipoParametro() != null)
				sql.append(" AND ID_TIPO_PARAMETRO=:idTipoParametro");
			if (entity.getNombre() != null && !entity.getNombre().equals(""))
				sql.append(" AND NOMBRE=:nombre");
			if (entity.getUsuarioCrea() != null && !entity.getUsuarioCrea().equals(""))
				sql.append(" AND USUARIO_CREA=:usuarioCrea");
			if (entity.getActivo() != null)
				sql.append(" AND ACTIVO=:activo");
			if (entity.getFechaCreacion() != null)
				sql.append(" AND FECHA_CREACION=:fechaCreacion");

			return super.findByCriteria(entity, sql.toString(), null, false);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroDAO.findByCriteria. Causa: " + e.getMessage() + ". Query: " + sql.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}

	@Override
	public TipoParametroDTO findByPK(TipoParametroDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("select" + " ID_TIPO_PARAMETRO," + " NOMBRE," + " USUARIO_CREA," + " ACTIVO," + " FECHA_CREACION"
					+ " FROM " + TABLA + " "

					+ " WHERE " + "ID_TIPO_PARAMETRO=:idTipoParametro");

			return (TipoParametroDTO) super.findByPK(entity, sql.toString());
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TipoParametroDAO.findByCriteria. Causa: " + e.getMessage() + ". Query: " + sql.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}

	@Override
	public List<TipoParametroDTO> findByCriteriaForeign(TipoParametroDTO entity) throws AppException {
		StringBuilder select = new StringBuilder("");
		StringBuilder where = new StringBuilder("");
		List<ForaneaDTO> listForaneaDTO = new ArrayList<>();
		try {
			select.append("" + " ID_TIPO_PARAMETRO," + " NOMBRE," + " USUARIO_CREA," + " ACTIVO," + " FECHA_CREACION");

			where.append(" WHERE 1=1 ");

			if (entity.getIdTipoParametro() != null)
				where.append(" AND TIPO_PARAMETRO.ID_TIPO_PARAMETRO=:idTipoParametro");
			if (entity.getNombre() != null && !entity.getNombre().equals(""))
				where.append(" AND TIPO_PARAMETRO.NOMBRE=:nombre");
			if (entity.getUsuarioCrea() != null && !entity.getUsuarioCrea().equals(""))
				where.append(" AND TIPO_PARAMETRO.USUARIO_CREA=:usuarioCrea");
			if (entity.getActivo() != null)
				where.append(" AND TIPO_PARAMETRO.ACTIVO=:activo");
			if (entity.getFechaCreacion() != null)
				where.append(" AND TIPO_PARAMETRO.FECHA_CREACION=:fechaCreacion");

			return super.findByCriteriaForeign(entity, TABLA, select.toString(), where.toString(), listForaneaDTO,
					entity.getAliasForaneas());
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error(
					"TipoParametroDAO.findByCriteria. Causa: " + e.getMessage() + ". Query: " + select.toString());
			throw exceptionService.throwException(ParametroConfig.CodigoExcepcion.GENERIC.getValue(), null);
		}
	}
}
