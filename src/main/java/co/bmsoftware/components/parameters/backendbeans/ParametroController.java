package co.bmsoftware.components.parameters.backendbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Qualifier;

import co.bmsoftware.components.parameters.config.ParametroConfig;
import co.bmsoftware.components.parameters.entites.ParametroDTO;
import co.bmsoftware.components.parameters.entites.TipoParametroDTO;
import co.bmsoftware.components.parameters.entites.ValorComponenteParametro;
import co.bmsoftware.components.parameters.services.api.IParametroService;
import co.bmsoftware.components.parameters.services.api.ITipoParametroService;
import co.bmsoftware.exception.AppException;
import co.bmsoftware.interfaces.services.base.IAppExceptionService;
import co.bmsoftware.managedbeans.DatosSesionBean;
import co.bmsoftware.managedbeans.MensajesBean;
import co.bmsoftware.utils.JSONUtil;
import co.bmsoftware.utils.Parameters;


/**
 * Clase ParametroController
 * 
 * @author bmsoftGenerator
 * @since V1.0
 */
@ManagedBean(name = "ParametroController")
@ViewScoped
public class ParametroController implements Serializable {
	private static final long serialVersionUID = 1L;
	protected static final Logger bitacora = LogManager.getLogger();

	@ManagedProperty(value = "#{mensajesBean}")
	private MensajesBean mensajes;
	@ManagedProperty(value = "#{DatosSesionBean}")
	private DatosSesionBean datosSesionBean;

	@ManagedProperty(value = "#{ParametroService}")
	private transient IParametroService objParametroService;
	@ManagedProperty(value = "#{TipoParametroService}")
	private transient ITipoParametroService objTipoParametroService;

	private List<SelectItem> itemsTipoParametro;

	private List<SelectItem> itemsActivo;
	/**
	 * Objeto para creacion/edicion
	 */
	private ParametroDTO objParametroSelected;
	/**
	 * Listado de registros obtenidos desde la BD
	 */
	private List<ParametroDTO> listEntitiesDTO;
	/**
	 * Objeto filtro para la busqueda
	 */
	private ParametroDTO objParametroFilter;
	/**
	 * Controlar si se esta creando un objeto o se esta editando
	 */
	private Boolean creating = false;

	private List<SelectItem> itemsEditor;

	@ManagedProperty(value = "#{ParmatroExceptionService}")
	@Qualifier("ParametroExceptionService")
	private IAppExceptionService exceptionService;

	@PostConstruct
	public void init() {
		try {
			cleanData();
			loadData();
			actionSearch();
		} catch (AppException e) {
			mensajes.mostrarMensaje(e);
		} catch (Exception e) {
			bitacora.error("AnexosConvocatoriaController.actionSearch. Causa: " + e.getMessage(), e);
			mensajes.mostrarMensaje(exceptionService.throwException(ParametroConfig.CodigoExcepcion.CONTROLLER.getValue(), null));
		}
	}

	/**
	 * Metodo para limpiar atributos de la clase
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 */
	public void cleanData() {
		objParametroFilter = new ParametroDTO();
		objParametroSelected = new ParametroDTO();
		listEntitiesDTO = new ArrayList<ParametroDTO>();
		creating = true;
	}

	/**
	 * Metodo para limpiar el objeto a guardaro o editar
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 */
	public void cleanObject() {
		objParametroSelected = new ParametroDTO();
		creating = true;
	}

	/**
	 * Metodo para cargar los datos iniciales
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 */
	private void loadData() throws Exception {
		itemsActivo = new ArrayList<SelectItem>();
		for (int i = 0; i < 2; i++) {
			SelectItem selectItem = new SelectItem();
			selectItem.setValue(i == 0 ? Parameters.ACTIVO : Parameters.INACTIVO);
			selectItem.setLabel(i == 0 ? "Si" : "No");
			itemsActivo.add(selectItem);
		}

		itemsTipoParametro = new ArrayList<SelectItem>();
		TipoParametroDTO filterTipoParametro = new TipoParametroDTO();
		filterTipoParametro.setActivo("S");
		List<TipoParametroDTO> listTipoParametro = objTipoParametroService.findByCriteria(filterTipoParametro);
		if (listTipoParametro != null && listTipoParametro.size() > 0) {
			for (TipoParametroDTO obj : listTipoParametro) {
				SelectItem selectItem = new SelectItem();
				selectItem.setValue(obj.getIdTipoParametro());
				selectItem.setLabel(obj.getNombre().toString());
				itemsTipoParametro.add(selectItem);
			}
		}
	}

	/**
	 * Evento de buscar
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 */
	public void actionSearch() {
		try {
			listEntitiesDTO = objParametroService.findByCriteria(objParametroFilter);

			if (listEntitiesDTO == null || listEntitiesDTO.size() == 0) {
				mensajes.mostrarMensajeBundle(MensajesBean.MENSAJE_ALERTA, Parameters.MENSAJE_TABLA_SIN_REGISTROS);
				return;
			}
		} catch (AppException e) {
			mensajes.mostrarMensaje(e);
		} catch (Exception e) {
			bitacora.error("ParametroController.actionSearch. Causa: " + e.getMessage(), e);
			mensajes.mostrarMensaje(exceptionService.throwException(ParametroConfig.CodigoExcepcion.CONTROLLER.getValue(), null));
		}
	}

	/**
	 * Evento asociado al boton guardar, puede ser para crear o editar un registro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 */
	public void actionSave() {
		try {
			if (creating) {
				actionCreate();
			} else {
				actionEdit();
			}

			creating = true;
		} catch (AppException e) {
			actionSearch();
			mensajes.mostrarMensaje(e);
		} catch (Exception e) {
			bitacora.error("ParametroController.actionSave. Causa: " + e.getMessage(), e);
			mensajes.mostrarMensaje(exceptionService.throwException(ParametroConfig.CodigoExcepcion.CONTROLLER.getValue(), null));
		}
	}

	/**
	 * Evento de crear un nuevo registro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 */
	public void actionCreate() throws Exception {

		if (objParametroSelected.getValorParametro().getValor() != null
				&& objParametroSelected.getValorParametro().getValor().size() > 0) {
			mensajes.mostrarMensajeError(ParametroConfig.ViewMessages.VALORES_FALTANTES.getDescripcion(), ParametroConfig.ViewMessages.VALORES_FALTANTES.getDescripcion());
			return;
		}

		validarParametro();

		if(objParametroSelected.getTipoValor().equals(ParametroConfig.TiposParametro.JSON.getValue())) {
			objParametroSelected.setValor(JSONUtil.convertirObjetoAJson(objParametroSelected.getValorParametro()));
		}
		objParametroSelected.setUsuarioCrea(datosSesionBean.getUserId());
		objParametroSelected.setFechaCreacion(new Date());
		objParametroService.create(objParametroSelected);
		objParametroSelected = new ParametroDTO();
		actionSearch();

		mensajes.mostrarMensajeBundle(MensajesBean.MENSAJE_OK, Parameters.MENSAJE_REGISTRO_GUARDADO);
	}

	/**
	 * Evento de editar un registro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 */
	public void actionEdit() throws Exception {
		if (objParametroSelected != null) {

			if (objParametroSelected.getTipoValor().equals(ParametroConfig.TiposParametro.JSON.getValue()) && (objParametroSelected.getValorParametro().getValor() == null
					|| objParametroSelected.getValorParametro().getValor().size() == 0)) {
				mensajes.mostrarMensajeError(ParametroConfig.ViewMessages.VALORES_FALTANTES.getDescripcion(), ParametroConfig.ViewMessages.VALORES_FALTANTES.getDescripcion());
				return;
			}

			validarParametro();
			if(objParametroSelected.getTipoValor().equals(ParametroConfig.TiposParametro.JSON.getValue())) {				
				objParametroSelected.setValor(JSONUtil.convertirObjetoAJson(objParametroSelected.getValorParametro()));
			}
//			objParametroSelected.setUsuarioCrea(datosSesionBean.getUserId());
			objParametroService.update(objParametroSelected);

			objParametroSelected = new ParametroDTO();
			actionSearch();
			mensajes.mostrarMensajeBundle(MensajesBean.MENSAJE_OK, Parameters.MENSAJE_REGISTRO_MODIFICADO);
		}
	}

	private void validarParametro() throws Exception {
		StringBuilder mensajeError = new StringBuilder();
		if(objParametroSelected.getTipoValor().equals(ParametroConfig.TiposParametro.JSON.getValue())) {
			if (objParametroSelected.getValor() != null && !objParametroSelected.getValor().isEmpty()) {		
				for (ValorComponenteParametro valor : objParametroSelected.getValorParametro().getValor()) {
					if(valor.getNombre() == null || valor.getNombre().equals("")) {
						mensajeError.append("El campo nombre de los valores en obligatorio.");
					}else if (valor.getValor() != null && !valor.getValor().equals("")) {
						if (!Pattern.matches(valor.getValoresPermitidos(), valor.getValor())) {
							mensajeError.append("No se ingreso un valor valido en el parametro " + valor.getNombre() + ". ");
						}
					}
				}
			}
		}else {
			if (objParametroSelected.getValor() == null || objParametroSelected.getValor().isEmpty()) {
				mensajeError.append("El campo valor debe tener información.");
			}
		}
		//SE MANDA LA EXCEPCIÓN CON EL ERROR GENERADO Y SE CAPTURA EN EL METODO ACTIOSAVE
		if (mensajeError.length() > 0) {
			throw new Exception(mensajeError.toString());
		}

	}
	

	public void eventAgregarValor() {
		objParametroSelected.getValorParametro().getValor().add(new ValorComponenteParametro());
	}

	/**
	 * Mostrar el dlg en modo edicion
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param item el objeto que se va a editar
	 */
	public void loadEdit(ParametroDTO object) {
		try {
			creating = false;
			objParametroSelected = object;
			objParametroSelected.getValorParametro();

			PrimeFaces.current().resetInputs(":formList:formPanelValor");
		} catch (Exception e) {
			mensajes.mostrarMensaje(e);
		}
	}

	/**
	 * Getters and setters
	 */
	public Boolean getCreating() {
		return creating;
	}

	public List<SelectItem> getItemsActivo() {
		return itemsActivo;
	}

	public void setExceptionService(IAppExceptionService exceptionService) {
		this.exceptionService = exceptionService;
	}
	
	public List<ParametroDTO> getListEntitiesDTO() {
		return listEntitiesDTO;
	}

	public void setListEntitiesDTO(List<ParametroDTO> listEntitiesDTO) {
		this.listEntitiesDTO = listEntitiesDTO;
	}

	public void setObjParametroSelected(ParametroDTO objParametroSelected) {
		this.objParametroSelected = objParametroSelected;
	}

	public ParametroDTO getObjParametroSelected() {
		return objParametroSelected;
	}

	public void setObjParametroFilter(ParametroDTO objParametroFilter) {
		this.objParametroFilter = objParametroFilter;
	}

	public ParametroDTO getObjParametroFilter() {
		return objParametroFilter;
	}

	public void setMensajes(MensajesBean mensajes) {
		this.mensajes = mensajes;
	}

	public void setObjParametroService(IParametroService objParametroService) {
		this.objParametroService = objParametroService;
	}

	public void setAppExceptionService(IAppExceptionService exceptionService) {
		this.exceptionService = exceptionService;
	}

	public void setDatosSesionBean(DatosSesionBean datosSesionBean) {
		this.datosSesionBean = datosSesionBean;
	}

	public void setObjTipoParametroService(ITipoParametroService objTipoParametroService) {
		this.objTipoParametroService = objTipoParametroService;
	}

	public void setItemsTipoParametro(List<SelectItem> itemsTipoParametro) {
		this.itemsTipoParametro = itemsTipoParametro;
	}

	public List<SelectItem> getItemsTipoParametro() {
		return itemsTipoParametro;
	}

	public List<SelectItem> getItemsEditor() {
		return itemsEditor;
	}

	public void setItemsEditor(List<SelectItem> itemsEditor) {
		this.itemsEditor = itemsEditor;
	}

}
