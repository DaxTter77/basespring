package co.bmsoftware.components.parameters.config;

public class ParametroConfig {

//	public static final String NOMBRE_TABLA_PARAM = "BECAS.PARAMETRO";
//	public static final String NOMBRE_TABLA_TIPO_PARAM = "BECAS.TIPO_PARAMETRO";
	public static final String NOMBRE_TABLA_PARAM = "PARAMETRO";
	public static final String NOMBRE_TABLA_TIPO_PARAM = "TIPO_PARAMETRO";

	public enum CodigoExcepcion {
		GENERIC("base01"), SERVICE("base02"), CONTROLLER("base03"), NOMBRE_REPETIDO("base04");

		private String value;

		private CodigoExcepcion(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
	
	public enum TiposParametro {
		TEXT("TEXT"), JSON("JSON"), HTML("HTML");
		
		private String value;
		
		private TiposParametro(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}
		
	}

	// Mensajes lanzados desde los controller por lo logica de presentacion
	public enum ViewMessages {
		VALORES_FALTANTES("Valores faltantes", "Se debe tener mínimo un valor para el parametro.");

		private String descripcion;
		private String solucion;

		private ViewMessages(String descripcion, String solucion) {
			this.descripcion = descripcion;
		}

		public String getDescripcion() {
			return descripcion;
		}

		public String getSolucion() {
			return solucion;
		}
	}

	public enum ComponentExceptions {
		BASE01(1, "Excepcion base01", "Por favor comuniquese con el administrador del sistema. $1",
				"Se presenta por algún problema al momento de comunicarse con la base de datos."),
		BASE02(2, "Excepcion base02", "Por favor comuniquese con el administrador del sistema. $1",
				"Se presenta por algún problema en la lógica de negocio."),
		BASE03(3, "Excepcion base03", "Por favor comuniquese con el administrador del sistema. $1",
				"Se presenta por algún problema en la comunicación con la pantalla."),
		BASE04(4, "Excepcion base04", "Por favor comuniquese con el administrador del sistema. $1",
				"Se presenta por algún dato repetido al momento de ingresar o modificar.");

		private int key;
		private String descripcion;
		private String solucion;
		private String soporte;

		private ComponentExceptions(int key, String descripcion, String solucion, String soporte) {
			this.key = key;
			this.descripcion = descripcion;
			this.solucion = solucion;
			this.soporte = soporte;
		}

		public int getKey() {
			return key;
		}

		public String getDescripcion() {
			return descripcion;
		}

		public String getSolucion() {
			return solucion;
		}

		public String getSoporte() {
			return soporte;
		}

	}

}
