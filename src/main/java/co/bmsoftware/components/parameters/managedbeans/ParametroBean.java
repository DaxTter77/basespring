
package co.bmsoftware.components.parameters.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.bmsoftware.components.parameters.entites.ParametroDTO;
import co.bmsoftware.components.parameters.entites.ValorParametro;
import co.bmsoftware.components.parameters.services.api.IParametroService;
import co.bmsoftware.utils.JSONUtil;

/**
 * Clase ParametroController
 * 
 * @author bmsoftGenerator
 * @since V1.0
 */
@ManagedBean(name = "ParametroBean")
@SessionScoped
public class ParametroBean implements Serializable {
	private static final long serialVersionUID = 1L;
	protected static final Logger bitacora = LogManager.getLogger();

	@ManagedProperty(value = "#{ParametroService}")
	private IParametroService objParametroService;
	/**
	 * Objeto para consulta
	 */
	private ParametroDTO parametroSelected;
	private String timeZone = "GMT-5";
	/**
	 * Listado de registros obtenidos desde la BD
	 */
	private List<ParametroDTO> listEntitiesDTO;

	@PostConstruct
	public void init() {
		loadData();
	}

	/**
	 * Metodo para cargar los datos iniciales
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 */
	private void loadData() {
	}

	/**
	 * Evento de buscar
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 */
	public String getParametro(int idParametro) {
		try {
			ParametroDTO entity = new ParametroDTO();
			entity.setIdParametro(Long.parseLong(idParametro + ""));
			return objParametroService.findByPK(entity).getValor();
		} catch (Exception e) {
			bitacora.error("ParametroController.getParametro. Causa: " + e.getMessage());
			return "";
		}
	}

	public ValorParametro getParametroJson(int idParametro) {
		try {
			ParametroDTO entity = new ParametroDTO();
			entity.setIdParametro(Long.parseLong(idParametro+""));
			String valor = objParametroService.findByPK(entity).getValor();
			return JSONUtil.convertirJsonAObjeto(valor, new ValorParametro());
		} catch (Exception e) {
			bitacora.error("ParametroController.getParametroJson. Causa: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Evento de modificar
	 * 
	 * @author Dv
	 */
	public void updateParametro(ParametroDTO parameterSelected) {
		try {
			objParametroService.update(parameterSelected);
		} catch (Exception e) {
			bitacora.error("ParametroController.actionUpdate. Causa: " + e.getMessage());
		}
	}

	public List<ParametroDTO> getListEntitiesDTO() {
		return listEntitiesDTO;
	}

	public void setListEntitiesDTO(List<ParametroDTO> listEntitiesDTO) {
		this.listEntitiesDTO = listEntitiesDTO;
	}

	public void setParametroSelected(ParametroDTO parametroSelected) {
		this.parametroSelected = parametroSelected;
	}

	public ParametroDTO getParametroSelected() {
		return parametroSelected;
	}

	public void setObjParametroService(IParametroService objParametroService) {
		this.objParametroService = objParametroService;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public void removeSessionBean(ActionEvent event) {
		try {
			String beanName = (String) event.getComponent().getAttributes().get("beanName");
			String uri = (String) event.getComponent().getAttributes().get("uri");
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(beanName);
			// Hacemos el redireccionamiento
			FacesContext.getCurrentInstance().getExternalContext().redirect(uri);
		} catch (Exception e) {
			bitacora.error("ParametroBean.removeSessionBean. Causa: " + e.getMessage());
		}
	}

	public void removeBean(String beanName) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(beanName);
	}
}
