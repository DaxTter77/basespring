package co.bmsoftware.components.parameters.entites;

public class ValorComponenteParametro {

	private String nombre;
	private String valor;
	private String descripcion;
	private String valoresPermitidos;
	private String editable;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getValoresPermitidos() {
		return valoresPermitidos;
	}

	public void setValoresPermitidos(String valoresPermitidos) {
		this.valoresPermitidos = valoresPermitidos;
	}

	public String getEditable() {
		return editable;
	}

	public void setEditable(String editable) {
		this.editable = editable;
	}

}
