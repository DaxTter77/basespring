
package co.bmsoftware.components.parameters.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import co.bmsoftware.components.parameters.config.ParametroConfig;
import co.bmsoftware.utils.JSONUtil;

public class ParametroDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	// objeto que contendra el aleas de las respectivas foraneas
	private HashMap<String, String> aliasForaneas;

	private Long idParametro;
	private BigDecimal idTipoParametro;
	private String nombre;
	private String descripcion;
	private String valor;
	private ValorParametro valorParametro;
	private String usuarioCrea;
	private Date fechaCreacion;
	private String tipoValor;
	private String nombreTipo;

	// Validaciones
	private Long idParametroValid;

	public HashMap<String, String> getAliasForaneas() {
		return aliasForaneas;
	}

	public ParametroDTO() {
	}

	// Metodos Get y Set de las variables de la clase

	public void setIdParametro(Long idParametro) {
		this.idParametro = idParametro;
	}

	public Long getIdParametro() {
		return idParametro;
	}

	public void setIdTipoParametro(BigDecimal idTipoParametro) {
		this.idTipoParametro = idTipoParametro;
	}

	public BigDecimal getIdTipoParametro() {
		return idTipoParametro;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getValor() {
		return valor;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public String getUsuarioCrea() {
		return usuarioCrea;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public ValorParametro getValorParametro() {
		try {
			if(this.getValor() != null && !this.tipoValor.equals(ParametroConfig.TiposParametro.JSON.getValue())) {
				return null;
			}
			if (this.valor != null) {
				if (this.valorParametro == null || this.valorParametro.getValor().size() == 0) {
					this.valorParametro = JSONUtil.convertirJsonAObjeto(this.valor, new ValorParametro());
				}
			} else if (this.valorParametro == null) {
				this.valorParametro = new ValorParametro();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valorParametro;
	}

	public void setValorParametro(ValorParametro valorParametro) {
		this.valorParametro = valorParametro;
	}

	public String getTipoValor() {
		return tipoValor;
	}

	public void setTipoValor(String tipoValor) {
		this.tipoValor = tipoValor;
	}

	public String getNombreTipo() {
		return nombreTipo;
	}

	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}

	public Long getIdParametroValid() {
		return idParametroValid;
	}

	public void setIdParametroValid(Long idParametroValid) {
		this.idParametroValid = idParametroValid;
	}

}
