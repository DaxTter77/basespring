package co.bmsoftware.components.parameters.entites;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ValorParametro implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nombre;
	private String descripcion;
	private List<ValorComponenteParametro> valor;

	public ValorParametro() {
		valor = new ArrayList<ValorComponenteParametro>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<ValorComponenteParametro> getValor() {
		return valor;
	}

	public void setValor(List<ValorComponenteParametro> valor) {
		this.valor = valor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
