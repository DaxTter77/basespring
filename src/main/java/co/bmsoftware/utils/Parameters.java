
package co.bmsoftware.utils;

/**
 * Clase para acceso a variables estaticas definidas.
 * 
 * @author jhon.andrey
 *
 */
public class Parameters {
	public static boolean AMBIENTE_SERVIDOR = false;

	public static final Boolean ENABLE = true;
	public static final Boolean DISABLE = false;
	public static final String ACTIVO = "S";
	public static final String INACTIVO = "N";

	// MENSAJES GENERICOS
	public static final String MENSAJE_TABLA_SIN_REGISTROS = "global.mensajes.sinregistros";
	public static final String MENSAJE_REGISTRO_GUARDADO = "global.mensajes.registro.crear";
	public static final String MENSAJE_REGISTRO_MODIFICADO = "global.mensajes.registro.modificar";
	public static final String MENSAJE_REGISTRO_ELIMINADO = "global.mensajes.registro.eliminar";

}
