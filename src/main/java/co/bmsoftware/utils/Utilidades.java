
package co.bmsoftware.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;

import co.bmsoftware.components.dao.persistence.GenericDAO;

public class Utilidades {

	@ManagedProperty(value = "GenericDAO")
	private static GenericDAO genericDAO;

	// Utilidades con cadenas
	public static class StringUtil {
		public static String delChar(String cadena) {
			int index = -1;
			char car;
			do {
				index++;
				car = cadena.charAt(index);
			} while (index < cadena.length() - 1 && car == 48);

			return cadena.substring(index, cadena.length());
		}

		// Elimina campos cero de la izquierda de la cadena
		public static String padLeft(String s, int n) {
			return String.format("%1$#" + n + "s", s);
		}

		// Elimina campos cero de la izquierda de la cadena
		public static String padLeftZero(Long d, int n) {
			// Formato: %Caracter a remplazar-Numero de digitos-digito
			String f = "%0" + n + "d";
			return String.format(f, d);

		}

		// Validaci?n campo es digito
		public static boolean isDigit(String str) throws Exception {
			if (str == null || str.length() == 0)
				return false;

			for (int i = 0; i < str.length(); i++) {

				// If we find a non-digit character we return false.
				if (!Character.isDigit(str.charAt(i)))
					return false;
			}
			return true;
		}

		// Validaci?n campo es alfabetico
		public static boolean isApha(String str) throws Exception {
			boolean val = true;
			List charsInvalidate = null;
			if (str == null)
				return false;

			charsInvalidate = new ArrayList();
			charsInvalidate = AccesoPropiedades.getListProperty("parametros.properties", "CHAR_UNSOPORT_DB", ",");

			for (int i = 0; i < charsInvalidate.size(); i++) {
				if (str.indexOf(charsInvalidate.get(i).toString()) != -1) {
					val = false;
					break;
				}
			}
			return val;
		}

		public static boolean isDateOrHour(String str) throws Exception {
			String[] hour;
			str = str.trim();

			if (str.length() < 8)
				return false;

			if (str != null && str.indexOf(":") != -1) {
				hour = str.split(":");

				if (hour.length != 3)
					return false;
				else if (hour[0].length() != 2 && hour[1].length() != 2 && hour[2].length() != 2)
					return false;
			}

			return true;
		}

		public static boolean isEmptyOrNull(String string) {
			if (string == null)
				return true;

			return string.equalsIgnoreCase("");
		}
	}

	public static class DatabaseUtils {
		public static Connection getConnection() throws Exception {
			return genericDAO.getDataConnection();
		}
	}

	public static class DateUtils {
		public static Date getFechaActualHoraCero() {
			Calendar hoy = Calendar.getInstance();
			hoy.set(Calendar.HOUR_OF_DAY, 0);
			hoy.set(Calendar.MINUTE, 0);
			hoy.set(Calendar.SECOND, 0);
			hoy.set(Calendar.MILLISECOND, 0);
			return hoy.getTime();
		}

		public static Date getFechaDiaHoraCero(int dia) {
			Calendar hoy = Calendar.getInstance();
			hoy.set(Calendar.HOUR_OF_DAY, 0);
			hoy.set(Calendar.MINUTE, 0);
			hoy.set(Calendar.SECOND, 0);
			hoy.set(Calendar.MILLISECOND, 0);
			hoy.set(Calendar.DAY_OF_MONTH, dia);
			return hoy.getTime();
		}

		public static Date truncDate(Date fecha) {
			Calendar fechaTruncated = Calendar.getInstance();
			fechaTruncated.setTime(fecha);
			fechaTruncated.set(Calendar.HOUR_OF_DAY, 0);
			fechaTruncated.set(Calendar.MINUTE, 0);
			fechaTruncated.set(Calendar.SECOND, 0);
			fechaTruncated.set(Calendar.MILLISECOND, 0);
			return fechaTruncated.getTime();
		}
	}

	public static class FileUtils {
		/**
		 * Metodo encargado de buscar un archivo en un directorio.
		 * 
		 * @param nombreArchivoBuscar
		 * @param rutaDirectorio
		 * @return
		 * @throws Exception
		 */
		public static boolean buscarArchivo(String nombreArchivoBuscar, String rutaDirectorio) throws Exception {
			boolean existe = false;
			if (nombreArchivoBuscar != null && !nombreArchivoBuscar.equals(null) && !nombreArchivoBuscar.equals("")
					&& rutaDirectorio != null && !rutaDirectorio.equals(null) && !rutaDirectorio.equals("")) {
				File directorio = new File(rutaDirectorio);

				// Se valida que se un un directorio.
				if (directorio.isDirectory()) {
					// Se recorren los archivos.
					File listaFile[] = directorio.listFiles();
					if (listaFile != null && listaFile.length > 0) {
						for (File file : listaFile) {
							if (file.getName().toLowerCase().trim().equals(nombreArchivoBuscar.toLowerCase())) {
								existe = true;
								break;
							}
						}
					}
				} else {
					System.out.println("NO ES UN DIRECTORIO VALIDO: " + rutaDirectorio);
				}
			}
			return existe;
		}

		/**
		 * Metodo encargado de obtner la extension del nombre de un arhcivo.
		 * 
		 * @param fileName
		 * @return
		 * @throws Exception
		 */
		public static String obtenerExtension(String fileName) throws Exception {
			String extension = null;

			int i = fileName.lastIndexOf('.');
			if (i > 0) {
				extension = fileName.substring(i + 1);
			}

			return extension;
		}

		/**
		 * Metodo encargado de obtner el nombre del archivo sin extension.
		 * 
		 * @param fileName
		 * @return
		 * @throws Exception
		 */
		public static String obtenerNombreSinExtension(String fileName) throws Exception {
			String nombre = null;

			int i = fileName.lastIndexOf('.');
			if (i > 0) {
				nombre = fileName.substring(0, i);
			}

			return nombre;
		}

		/*
		 * Metodo para consultar los archivos de una extension especifica en un
		 * directorio
		 */
		public static String[] filesFilteredByExtension(String folder, String ext) {
			Utilidades.FileUtils um = new Utilidades.FileUtils();
			GenericExtFilter filter = um.new GenericExtFilter(ext);

			File dir = new File(folder);

			if (dir.isDirectory() == false) {
				System.out.println("Directory does not exists : " + folder);
				return null;
			}

			// list out all the file name and filter by the extension
			return dir.list(filter);
		}

		public class GenericExtFilter implements FilenameFilter {

			private String ext;

			public GenericExtFilter(String ext) {
				this.ext = ext;
			}

			public boolean accept(File dir, String name) {
				return (name.endsWith(ext));
			}
		}

		/**
		 * Metodo encargado de eliminar un archivo.
		 * 
		 * @param file
		 * @throws Exception
		 */
		public static boolean eliminarArchivo(String file) throws Exception {
			if (file != null && !file.equals(null) && !file.equals("")) {
				File deleteFile = new File(file);
				if (deleteFile != null && deleteFile.isFile()) {
					return deleteFile.delete();
				}
			}
			return false;
		}

		/**
		 * Metodo encargado de validar si existe un archivo.
		 * 
		 * @param archivo
		 * @return
		 * @throws Exception
		 */
		public static boolean existeArchivo(String archivo) throws Exception {
			File file = new File(archivo);
			return (file != null && file.isFile());
		}

		/**
		 * Metodo encargado de crear un archivo.
		 * 
		 * @param ruta
		 * @param contenido
		 * @throws Exception
		 */
		public static void crearArchivo(String ruta, String contenido) throws Exception {
			BufferedWriter out = new BufferedWriter(new FileWriter(ruta));
			out.write(contenido);
			out.close();
		}

		/* Metodo que retorna la lineas que tienen una longitud a la enviada */
		public static List<Integer> validarLongitud(File file, int longitud) throws Exception {
			String line;
			List<Integer> lineas = new ArrayList<Integer>();
			int i = 0;
			try (BufferedReader br = new BufferedReader(new FileReader(file))){
				while ((line = br.readLine()) != null) {
					i++;
					// System.out.println(i+"."+line.substring(11,22));
					if (line.length() != longitud) {
						// System.out.println(i+"-Longitud: "+line.length());
						lineas.add(i);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return lineas;
		}

		/* Metodo que retorna la lineas que tienen una longitud a la enviada */
		public static List<String> obtenerContenidoArchivo(File file, int posicionInicial, int posicionFinal)
				throws Exception {
			String line;
			List<String> lineas = new ArrayList<String>();
			int i = 0;
			try (BufferedReader br = new BufferedReader(new FileReader(file))){
				while ((line = br.readLine()) != null) {
					i++;
					// System.out.println(i+"."+line.substring(11,22));
					if (line.length() != 0) {
						try {
							lineas.add(line.substring(posicionInicial, posicionFinal));
						} catch (Exception e) {
							System.out.println("ERROR LINEA: " + i);
							e.printStackTrace();
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return lineas;
		}

		/*
		 * Metodo que valida que identifica los regisros repetidos en un cargue
		 */
		public static List<Integer> validarRegistros(File file) throws Exception {
			List<Integer> lineas = new ArrayList<Integer>();
			try(BufferedReader br = new BufferedReader(new FileReader(file))){
				String line;
				
				int i = 0;
				HashMap control = new HashMap();
				while ((line = br.readLine()) != null) {
					i++;
					if (control.get(line.substring(0, 36)) != null) {
						lineas.add(i);
					} else {
						control.put(line.subSequence(0, 36), "P");
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			return lineas;
		}
	}

	// Utilidades con cadenas
	public static class EncryptionUtil {
		// some random salt
		private static final byte[] SALT = { (byte) 0x22, (byte) 0x21, (byte) 0xF1, (byte) 0x54, (byte) 0xC1,
				(byte) 0x9E, (byte) 0x5B, (byte) 0x74 };

		private final static int ITERATION_COUNT = 24;

		public static String encode(String input) {
			if (input == null) {
				throw new IllegalArgumentException();
			}
			try {

				KeySpec keySpec = new PBEKeySpec(null, SALT, ITERATION_COUNT);
				AlgorithmParameterSpec paramSpec = new PBEParameterSpec(SALT, ITERATION_COUNT);

				SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

				Cipher ecipher = Cipher.getInstance(key.getAlgorithm());
				ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);

				byte[] enc = ecipher.doFinal(input.getBytes());

				String res = new String(Base64.encodeBase64(enc));
				// escapes for url
				res = res.replace('+', '-').replace('/', '_').replace("%", "%25").replace("\n", "%0A");

				return res;

			} catch (Exception e) {
			}

			return "";

		}

		public static String decode(String token) {
			if (token == null) {
				return null;
			}
			try {

				String input = token.replace("%0A", "\n").replace("%25", "%").replace('_', '/').replace('-', '+');

				byte[] dec = Base64.decodeBase64(input.getBytes());

				KeySpec keySpec = new PBEKeySpec(null, SALT, ITERATION_COUNT);
				AlgorithmParameterSpec paramSpec = new PBEParameterSpec(SALT, ITERATION_COUNT);

				SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

				Cipher dcipher = Cipher.getInstance(key.getAlgorithm());
				dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

				byte[] decoded = dcipher.doFinal(dec);

				String result = new String(decoded);
				return result;

			} catch (Exception e) {
				// use logger in production code
				e.printStackTrace();
			}

			return null;
		}
	}

	public static class WebUtil {
		private static final int DEFAULT_BUFFER_SIZE = 10240;

		// PUBLICAR SOLO METODOS RELACIONADOS CON OBJETOS WEB

		/**
		 * Metodos utilitarios para obtener un ManagedBean.
		 * 
		 * @param beanName
		 * @return
		 */
		public static Object getManagedBean(String theBeanName) {
			final Object returnObject = FacesContext.getCurrentInstance().getELContext().getELResolver()
					.getValue(FacesContext.getCurrentInstance().getELContext(), null, theBeanName);
			return returnObject;
		}

		/**
		 * Remove the managed bean based on the bean name.
		 *
		 * @param beanName the bean name of the managed bean to be removed
		 */
		public static void resetManagedBean(String beanName) {
			FacesContext.getCurrentInstance().getELContext().getELResolver()
					.setValue(FacesContext.getCurrentInstance().getELContext(), null, beanName, null);
		}

		/**
		 * Get parameter value from request scope.
		 *
		 * @param name the name of the parameter
		 * @return the parameter value
		 */
		public static String getRequestParameter(String name) {
			return (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(name);
		}

		/**
		 * Metodo encargado de registrar un objeto en la sesion.
		 * 
		 * @param attribute
		 * @param object
		 */
		public static void setSessionValue(String attribute, Object object) {
			getServletRequest().getSession(true).setAttribute(attribute, object);
		}

		/**
		 * Metodo encargado de obtener un objeto de la sesion.
		 * 
		 * @param attribute
		 * @return
		 */
		public static Object getSessionValue(String attribute) {
			Object object = getServletRequest().getSession(true).getAttribute(attribute);
			return object;
		}

		private static HttpServletRequest getServletRequest() {
			return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		}

		public static void retornarArchivo(FacesContext facesContext, File file, String formato) throws Exception {

			BufferedInputStream input = null;
			BufferedOutputStream output = null;
			ExternalContext externalContext = facesContext.getExternalContext();

			try {

				// Open file.
				input = new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE);

				// Init servlet response.
				externalContext.responseReset();
				externalContext.setResponseContentType("application/" + formato);
				externalContext.setResponseContentLength((int) file.length());
				externalContext.setResponseHeader("Content-Disposition",
						"attachment; filename=\"" + file.getName() + "\"");
				output = new BufferedOutputStream(externalContext.getResponseOutputStream(), DEFAULT_BUFFER_SIZE);

				// Write file contents to response.
				byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
				int length;
				while ((length = input.read(buffer)) > 0) {
					output.write(buffer, 0, length);
				}
			} finally {
				try {
					if (output != null) {
						output.close();
					}
					if (input != null) {
						input.close();
					}
					if (file != null) {
						file.delete();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			facesContext.responseComplete();
		}
	}
}