package co.bmsoftware.utils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

public class JSONUtil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Convertir un JSON en un objecto
	 * 
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static Object castToObject(JSONObject myjson, Class clase)
			throws InstantiationException, IllegalAccessException {
		Object result = clase.newInstance();
		Field[] atrib = result.getClass().getDeclaredFields();

		for (int i = 0; i < atrib.length; i++) {
			try {
				atrib[i].setAccessible(true);
				atrib[i].set(result, myjson.get(atrib[i].getName()));
			} catch (Exception e) {
				System.out.println("error: " + atrib[i].getName());
			}
		}

		return result;
	}

	public static String convertirObjetoAJson(Object objeto) throws Exception { // Se recibe como parametro el objeto de
		// tipo Object por lo tanto puede tomar
		// cualquier tipo de objeto.
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(objeto);
	}

	// Metodo para convertir una cadena json a objeto.
	public static <T> T convertirJsonAObjeto(String json, T objeto) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		return (T)mapper.readValue(json, objeto.getClass());
	}

	public static JsonNode getJsonNode(String json) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readTree(json);
	}

	// Metodo para convertir una cadena json a una coleccion.
	public static <T> List<T> convertirJsonAColeccion(String json, T clase) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		CollectionType javaType = mapper.getTypeFactory().constructCollectionType(List.class, clase.getClass());
		return mapper.readValue(json, javaType);
	}

	public static Map<String, Object> getMapFromJson(String json) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, new TypeReference<Map<String, Object>>() {
		});
	}
}
