
package co.bmsoftware.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class AccesoPropiedades {
	public AccesoPropiedades() {
	}

	public static String getParametro(String nombreParametro) throws Exception {
		return getProperty("parametros.properties", nombreParametro);
	}

	public static List getParametroLista(String nombreParametro, String separador) throws Exception {
		return getListProperty("parametros.properties", nombreParametro, separador);
	}

	public static String getMensaje(String codigoMensaje) throws Exception {
		return getProperty("mensajes.properties", codigoMensaje);
	}

	private static String getProperty(String fileName, String propertyName) throws Exception {
		Properties pro = null;
		String valor;
		try {
			// Si el archivo es el de parametros y estamos en ambiente local cambiamos el
			// nombre al archivo
			if (fileName.equals("parametros.properties") && !Parameters.AMBIENTE_SERVIDOR) {
				fileName = "parametros_local.properties";
			}

			pro = new Properties();
			InputStream input = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("co/bmsoft/estandar/vista/properties/" + fileName);
			pro.load(input);

			valor = pro.getProperty(propertyName);

			pro.clear();

			input.close();

			return valor;
			// return pro.getProperty(propertyName);
		} catch (IOException ex) {
			System.out.println("Error leyendo propiedad por: " + ex.getMessage());
			throw ex;
		}
	}

	public static List getListProperty(String fileName, String propertyName, String propertySeparator)
			throws Exception {
		Properties pro = null;
		List list;
		String[] d;

		try {
			// Si el archivo es el de parametros y estamos en ambiente local cambiamos el
			// nombre al archivo
			if (fileName.equals("parametros.properties") && !Parameters.AMBIENTE_SERVIDOR) {
				fileName = "parametros_local.properties";
			}

			pro = new Properties();
			InputStream input = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("co/bmsoft/estandar/vista/properties/" + fileName);
			pro.load(input);

			list = new ArrayList();
			d = pro.getProperty(propertyName).split(propertySeparator);

			for (int i = 0; i < d.length; i++)
				list.add(d[i]);

			pro.clear();

			input.close();

			return list;

		} catch (IOException ex) {
			System.out.println("Error leyendo propiedad por: " + ex.getMessage());
			throw ex;
		}
	}

}
