
package co.bmsoftware.exception;

public class AppException extends Exception {
	private static final long serialVersionUID = 1L;
	// DATOS EXCEPCION NO CONTROLADA
	public static final String CODIGO_EXCEPCION = "0";
	public static final String DESCRIPCION_EXCEPCION = "En este momento no es posible procesar su solicitud, por: ";
	public static final String SOLUCION_EXCEPCION = "Intentelo de nuevo o comuniquese con el administrador del sistema";
	// DATOS EXCEPCION CON CODIGO DE ERROR INVALIDO
	public static final String DESCRIPCION_EXCEPCION_CODIGO = "El codigo de error $1 no existe";
	public static final String SOLUCION_EXCEPCION_CODIGO = "Por favor informe esta situaci�n al administrador del sistema";
	// EXCEPCIONES
	public static final String CODIGO_EXCEPCION_GENERIC = "base01";
	public static final String CODIGO_EXCEPCION_SERVICE = "base02";
	public static final String CODIGO_EXCEPCION_CONTROLLER = "base03";
	public static final String CODIGO_EXCEPCION_NOMBRE_REPETIDO = "base04";

	/**
	 * Codigo del Exception
	 */
	private String codigo;
	/**
	 * Descripcion del Exception
	 */
	private String descripcion;
	/**
	 * Solucion sugerida al Exception
	 */
	private String solucion;
	/**
	 * Descripcion mas detallada del Exception para el personal de soporte
	 */
	private String soporte;
	/**
	 * Datos para el reemplazo de los comodines en la descripcion del error
	 */
	private String[] dato;

	public AppException() {
	}

	/**
	 * Construir a parte de un Exception
	 * 
	 * @author wilferac
	 * @since
	 * @param e
	 */
	public AppException(Exception e) {
		codigo = CODIGO_EXCEPCION;
		descripcion = DESCRIPCION_EXCEPCION + e.getMessage();
		solucion = SOLUCION_EXCEPCION;
	}

	/**
	 * @author wilferac
	 * @since
	 * @param codigo codigo del error
	 * @param dato   datos para el reemplazo de comodines en la descripcion
	 * @throws Exception
	 */
	public AppException(String codigo, String[] dato) {
		this.codigo = codigo;
		this.dato = dato;
	}

	/**
	 * Reemplazar datos en la descripcion
	 * 
	 * @author wilferac
	 * @since
	 */
	public void loadData() {
		// Buscamos si hay datos para asignarle al mensaje
		if (dato != null) {
			for (int i = 0; i < dato.length; i++) {
				if (descripcion.indexOf("$" + (i + 1)) != -1) {
					descripcion = descripcion.replaceAll("\\$" + (i + 1), dato[i]);
				}
			}
		}
	}

	/** Setters and Getters **/
	public String[] getDato() {
		return dato;
	}

	public void setDato(String[] dato) {
		this.dato = dato;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setSolucion(String solucion) {
		this.solucion = solucion;
	}

	public void setSoporte(String soporte) {
		this.soporte = soporte;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getSolucion() {
		return solucion;
	}

	public String getSoporte() {
		return soporte;
	}
}
