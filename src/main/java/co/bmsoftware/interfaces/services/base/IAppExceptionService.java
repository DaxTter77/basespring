
package co.bmsoftware.interfaces.services.base;

import co.bmsoftware.exception.AppException;

public interface IAppExceptionService {

	public AppException throwException(String codigo, String[] dato);

}
