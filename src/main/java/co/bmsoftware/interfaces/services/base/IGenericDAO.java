package co.bmsoftware.interfaces.services.base;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.bmsoftware.components.dao.entities.ForaneaDTO;

public interface IGenericDAO {

	void create(Object entity, String sql) throws Exception;

	Long create(Object entity, String sql, String[] columnNames) throws Exception;

	void update(Object entity, String sql) throws Exception;

	void delete(Object entity, String sql) throws Exception;

	List findByCriteria(Object entity, String sql, String order, boolean isDesc) throws Exception;

	Object findByPK(Object entity, String sql) throws Exception;

	Connection getDataConnection() throws Exception;

	List<Map<String, Object>> findByCriteriaGeneral(String sql, Map<String, ?> parametros) throws Exception;

	Map<String, Object> findByPKGeneral(String sql, Map<String, ?> parametros) throws Exception;

	List findByCriteriaForeign(Object entity, String tabla, String select, String where, List<ForaneaDTO> foraneasList,
			HashMap<String, String> aliasForaneas) throws Exception;

	List executeParametizedQuery(Object entity, Long idParametro) throws Exception;

}